---
- name: Configure Docker (geerlingguy.docker)
  include_role:
    name: geerlingguy.docker
  when:
    - ansible_facts['distribution'] != "Amazon"
    - not offline_setup or (docker_add_repo is defined and not docker_add_repo) or (docker_repo_url is defined) # Only attempt in offline setups if user is using custom repo or providing custom repo URL

- name: Configure Docker (RHEL - Amazon Linux)
  shell: "{{ 'amazon-linux-extras' if ansible_facts['distribution_major_version'] == '2' else 'yum' }} install docker -y && systemctl enable docker && systemctl start docker"
  when: ansible_facts['distribution'] == "Amazon"

- name: Configure Node Exporter (geerlingguy.node_exporter)
  include_role:
    name: geerlingguy.node_exporter
  when: not offline_setup or node_exporter_download_url is defined

- name: Configure Kernel vm.max_map_count setting
  sysctl:
    name: vm.max_map_count
    value: '262144'
    sysctl_set: true
    state: present
    reload: true
  tags: sysctl

- name: Remove old Opensearch container
  docker_container:
    name: opensearch
    state: absent
  tags:
    - reconfigure
    - restart

- name: Start Opensearch container(s)
  docker_container:
    name: opensearch
    image: "{{ opensearch_docker_image }}"
    pull: true
    restart_policy: always
    state: started
    env:
      OPENSEARCH_JAVA_OPTS: "-Xms{{ opensearch_heap_size }}m -Xmx{{ opensearch_heap_size }}m"
      bootstrap.memory_lock: 'true'
      cluster.initial_cluster_manager_nodes: "{{ ((opensearch_int_addrs | map('split', '.') | map('first') | list) if internal_addr_use_hostnames else opensearch_int_addrs) | join(',') }}"
      discovery.seed_hosts: "{{ opensearch_int_addrs | join(',') }}"
      DISABLE_INSTALL_DEMO_CONFIG: 'true'
      DISABLE_SECURITY_PLUGIN: 'true'
    volumes:
      - gitlab_search_osdata:/usr/share/opensearch/data
    ulimits: memlock:-1:-1
    network_mode: host
  register: result
  retries: 2
  delay: 5
  until: result is success
  tags:
    - reconfigure
    - restart

- name: Wait for Opensearch container(s) to be available
  uri:
    url: 'http://localhost:9200/_cluster/health?wait_for_status=yellow&timeout=60s'
    timeout: 60
  register: result
  until: result.status == 200
  retries: 20
  delay: 5
  tags:
    - reconfigure
    - restart

- name: Prune any dangling Docker Images
  docker_prune:
    images: true
    timeout: 180
  when: opensearch_docker_prune
  tags: reconfigure
