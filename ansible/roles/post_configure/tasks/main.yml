- name: End run if running against Geo secondary site
  meta: end_play
  when:
    - (geo_secondary_site_group_name in groups)

- name: Wait for GitLab to be available
  uri:
    url: '{{ external_url_sanitised }}/-/readiness'
    validate_certs: false
    timeout: 60
  register: result
  until: result.status == 200
  retries: 30
  delay: 10
  tags: healthcheck

- name: Get details for running GitLab Charts Toolbox Pod
  block:
    - name: Configure local kubeconfig to point to correct cluster
      include_role:
        name: gitlab_charts
        tasks_from: kubeconfig
        apply:
          become: false
          delegate_to: localhost
          run_once: true

    - name: Check Toolbox Deployment & Pod is ready
      shell: |
        kubectl wait --for=condition=available --timeout=360s deployment -n {{ gitlab_charts_release_namespace }} -l release=gitlab -l app=toolbox
        kubectl wait --for=condition=ready --timeout=360s pod -l app=toolbox,'!job-name' -n {{ gitlab_charts_release_namespace }}

    - name: Retrieve Toolbox pod details
      kubernetes.core.k8s_info:
        kind: Pod
        label_selectors:
          - app=toolbox
      register: toolbox_pod_list

    - name: Save Toolbox pod name
      set_fact:
        toolbox_pod: "{{ toolbox_pod_list | json_query('resources[0].metadata.name') }}"
      when: toolbox_pod_list.resources | length != 0
  when:
    - cloud_native_hybrid_environment
    - "'gitlab_rails' not in groups"
  tags:
    - license
    - reconfigure
    - opensearch

- name: Get and save Environment Settings and License details
  block:
    - name: Get and save Environment Settings and License plan via GitLab Rails
      command: |
        gitlab-rails runner "
          env_settings = ApplicationSetting.last.attributes.select {|k,v| !k.include?('encrypted') }
          env_settings['current_license_plan'] = License.current&.plan

          print env_settings.to_json
        "
      delegate_to: "{{ groups['gitlab_rails'][0] }}"
      delegate_facts: true
      become: true
      register: env_settings_response_rails
      when: "'gitlab_rails' in groups"

    - name: Save Environment Settings
      set_fact:
        gitlab_environment_settings: '{{ env_settings_response_rails.stdout | from_json }}'
      when: "'gitlab_rails' in groups"

    - name: Get and save Environment Settings and License plan via GitLab Toolbox pod
      kubernetes.core.k8s_exec:
        pod: "{{ toolbox_pod }}"
        namespace: "{{ gitlab_charts_release_namespace }}"
        command: |
          gitlab-rails runner "
            env_settings = ApplicationSetting.last.attributes.select {|k,v| !k.include?('encrypted') }
            env_settings['current_license_plan'] = License.current&.plan

            print env_settings.to_json
          "
      register: env_settings_response_pod
      when:
        - toolbox_pod is defined
        - "'gitlab_rails' not in groups"

    - name: Save Environment Settings
      set_fact:
        gitlab_environment_settings: '{{ env_settings_response_pod.stdout | from_json }}'
      when:
        - toolbox_pod is defined
        - "'gitlab_rails' not in groups"
  tags:
    - license
    - reconfigure
    - opensearch

- name: Perform GitLab post configuration
  block:
    - name: Configure License
      import_tasks: license.yml
      when:
        - gitlab_environment_settings.current_license_plan not in ['premium', 'ultimate']
        - gitlab_edition != 'gitlab-ce'
      tags:
        - license
        - opensearch

    - name: Configure any required settings via API
      import_tasks: configure.yml
      tags: reconfigure

    - name: Configure Advanced Search
      import_tasks: advanced_search.yml
      when:
        - ('opensearch' in groups or advanced_search_external)
        - gitlab_environment_settings.current_license_plan in ['premium', 'ultimate']
        - gitlab_edition != 'gitlab-ce'
      tags: opensearch

- name: Run Custom Tasks
  block:
    - name: Check if Custom Tasks file exists
      stat:
        path: "{{ post_configure_custom_tasks_file }}"
      register: post_configure_custom_tasks_file_path
      delegate_to: localhost
      become: false

    - name: Run Custom Tasks
      include_tasks:
        file: "{{ post_configure_custom_tasks_file }}"
        apply:
          tags: custom_tasks
      when: post_configure_custom_tasks_file_path.stat.exists
  tags: custom_tasks
